<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Desastres</title>
</head>
<body>
	<h1>Lista de desastres</h1>
	
	<div>
   		<a href="<c:url value="/desastre/agregar"/>" class="btn btn-success">Agregar</a>
   	</div><br>
	
	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>Fecha</th>
				<th>Tipo</th>
				<th>Distrito</th>
				<th>Nro. de Damnificados</th>
				<th>Acciones</th>
			</tr>
		</thead>				
		<tbody>			
			<c:forEach var = "item" items="${lista}">
				<tr>
					<td><c:out value = "${item.id}"/></td>
					<td><c:out value = "${item.fecha}"/></td>
					<td><c:out value = "${item.tipo}"/></td>
					<td><c:out value = "${item.distrito}"/></td>
					<td><c:out value = "${item.nroDamn}"/></td>
					<td>
						<div class="col-sm-6">
							<a href="<c:url value="/desastre/editar?id=${item.id}"/>">Editar</a>
						</div>
   						    			
						<div class="col-sm-6">									
							<a href="<c:url value="/desastre/eliminar?id=${item.id}"/>">Eliminar</a>
						</div>
					</td>
				</tr>
	      	</c:forEach>
		</tbody>
	</table>	
	
</body>
</html>