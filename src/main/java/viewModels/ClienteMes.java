package viewModels;

public class ClienteMes {
	private String mes;
	private int nroDesastres;
	
	public ClienteMes(String mes, int nroDesastres) {
		super();
		this.mes = mes;
		this.nroDesastres = nroDesastres;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public int getNroDesastres() {
		return nroDesastres;
	}

	public void setNroDesastres(int nroDesastres) {
		this.nroDesastres = nroDesastres;
	}		
}
