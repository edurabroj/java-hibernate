package com.solweb.hibernate;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import entities.Desastre;
import data.IDao;
import validators.DesastreValidator;

@Controller
@RequestMapping("desastre")
public class DesastreController {
	private String carpetaVistas = "desastre/";
	private String redirectUrl = "redirect:/desastre";
	
	@Autowired
	private IDao dao;
	
	private DesastreValidator desastreValidator;
	
	public DesastreController() {
		desastreValidator = new DesastreValidator();
	}
	
	@RequestMapping("")
	public ModelAndView index(HttpServletRequest req) {	
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "index");
		List<Desastre> listaDesastre = dao.obtenerTodos(Desastre.class);
		mav.addObject("lista",listaDesastre);
		return mav;
	}
	
	@RequestMapping("/agregar")
	public ModelAndView agregar(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "form");
		mav.addObject("objeto", new Desastre());
		return mav;
	}
	
	@RequestMapping(value="/agregar", method=RequestMethod.POST)
	public ModelAndView agregar
		(
				@ModelAttribute ("objeto") Desastre objeto,
				BindingResult result
		)
	{
		desastreValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(carpetaVistas + "form");
			mav.addObject("objeto", objeto);
			return mav;		
		}else {
			dao.guardar(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.GET)
	public ModelAndView editar(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(carpetaVistas + "form");
		
		int id = Integer.parseInt(request.getParameter("id"));
		mav.addObject("objeto", dao.obtenerPorId(Desastre.class, id));
		return mav;
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.POST)
	public ModelAndView editar
	(
		@ModelAttribute ("objeto") Desastre objeto,
		BindingResult result
	)
	{
		desastreValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(carpetaVistas + "form");
			mav.addObject("objeto", objeto);
			return mav;		
		}else {
			dao.guardar(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/eliminar", method=RequestMethod.GET)
	public ModelAndView eliminar(HttpServletRequest request)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		dao.eliminar(Desastre.class, id);
		return new ModelAndView(redirectUrl);
	}
}
