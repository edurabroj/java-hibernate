package validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import data.Dao;
import data.IDao;
import entities.Cliente;
import entities.Desastre;

public class ClienteValidator implements Validator {
	@Autowired
	private IDao dao;
	
	@Override
	public boolean supports(Class<?> type) {
		return Cliente.class.isAssignableFrom(type);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Cliente objeto = (Cliente) o;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "required.nombre","El nombre es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "aPaterno", "required.aPaterno","El apellido paterno es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "aMaterno", "required.aMaterno","El apellido materno es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fechaNac", "required.fechaNac","La fecha de nacimiento es obligatoria");
		
		if(dao.consultar("from Cliente c where c.nombre='"+ objeto.getNombre() + "' and c.id !=" + objeto.getId() ).size()>0) {
			errors.rejectValue("nombre", "nombreRepetido", "El nombre ya est� registrado en la base de datos.");
		}
	}
}